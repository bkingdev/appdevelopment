states = {
    "Oregon": "OR",
    'Florida':'FL',
    'California':'CA',
    'New York' : 'NY',
    'Michigan' : 'MI'
}

cities = {
    'CA':'San Francisco',
    'MI':'Detroit',
    'FL':'Jacksonville'
}

cities['NY'] = 'New York'
cities['OR'] = 'Portland'

print '-'*10

print 'NY state has : ', cities['NY']

print "-"*10

print "Michigan's abbreviation is ", states["Michigan"]

print "Michigan has : ", cities[states["Michigan"]]

for abbrev, city in cities.items():
    print "%s has the city %s" %(abbrev, city)
    
state = states.get('Texas', None)

if not state:
    print "No texas"
    
city = cities.get('TX', 'DOES NOT EXIST')
print "The city for the state 'TX' is: %s" %city


    
