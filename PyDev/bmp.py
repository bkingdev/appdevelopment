#!/usr/bin/env python

def write_greyscale(filename, pixels):
    """Creates and writes a grayscale BMP file.

    Args:
        filename : name of file
        pixels: a rectangular images stores as a sequence of rows.
            Each row must be iterable series of integers in the
            range 0-255

    Raises: OSError: If the file couldn't be written
    """
    height = len(pixels)
    width = len(pixels[0])

    with open(filename, 'wb') as bmp:

        bmp.write(b'BM')

        size_bookmark = bmp.tell() # The next four byes hold file size
        bmp.write(b'\x00\x00\x00\x00') #little endian integer

        bmp.write(b'\x00\x00')
        bmp.write(b'\x00\x00')

        pixel_offset_bookmark = bmp.tell()
        bmp.write(b'\x00\x00\x00\x00') #zero placeholder for now

        # Image Header
        bmp.write(b'\x28\x00\x00\x00')
        bmp.write(_int32_to_bytes(width)) #image width in pixels
        bmp.write(_int32_to_bytes(height)) #image height in pixels
        bmp.write(b'\x01\x00') # Number of image planes
        bmp.write(b'\x08\x00') # Bites per pixel 8 for grayscale
        bmp.write(b'\x00\x00\x00\x00') # No compression
        bmp.write(b'\x00\x00\x00\x00') #Zero for uncompressed imgs 
        bmp.write(b'\x00\x00\x00\x00')  #Unused pixels per meter
        bmp.write(b'\x00\x00\x00\x00')  #Unused pixels per meter
        bmp.write(b'\x00\x00\x00\x00')  #Use whole color table
        bmp.write(b'\x00\x00\x00\x00')  #All colors are important
        
        #color palette
        for c in range(256):
            bmp.write(bytes((c ,c, c, 0))) #BGRZ(ero)
        
        #Pixel data
        pixel_data_bookmark = bmp.tell()
        for row in reversed(pixels):
            row_data = bytes(row)
            bmp.write(row_data)
        #End of file
        eof_bookmark = bmp.tell()
        #Fill in the placeholder
        bmp.seek(size_bookmark)
        bmp.write(_int32_to_bytes(eof_bookmark))

        #Fill in pixel offset placeholder
        bmp.seek(pixel_offset_bookmark)
        bmp.write(_int32_to_bytes(pixel_data_bookmark))

def _int32_to_bytes(i):
    return bytes( (i & 0xff,
                   i >> 8 & 0xff,
                   i >> 16 & 0xff,
                   i >> 24 & 0xff) )

    
