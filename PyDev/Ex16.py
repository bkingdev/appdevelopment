from sys import argv

script, filename = argv

print "We're going to erase %r" % filename
print "If you don't want that, hit CTRL-C (^C)."
print "If you do want that, hit RETURN."

raw_input("?")

print "Opening the file..."
target = open(filename, "w")

print "Truncating the file..."
target.truncate()

print "Now I'm going to ask you for 3 lines..."

line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")
lines = [line1, line2, line3]

print "I'm going to write these to file."

for line in lines:
	target.write(line+"\n")
print "And Finally, Close It"
target.close()


