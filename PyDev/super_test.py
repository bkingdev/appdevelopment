#!/usr/bin/env python

class A(object):

    def __init__(self, name):
        self.name = name

    def routine(self):
        print "A.routine()"

class B(A):

    def __init__(self, bob, blanda):
        self.bob = bob
        self.blanda = blanda

    def routine(self):
        print self.name

b = B('brad', 'smanda')
b.routine()



