
generation_range = 10
multiplier = 5

def my_generator():
    myList = range(generation_range)
    for i in myList:
        yield i * multiplier

thisList = my_generator()

for i in thisList:
    print i




