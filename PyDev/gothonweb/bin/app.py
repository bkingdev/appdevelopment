#!/usr/bin/env python

import web

urls = ('/', 'index'), ('/name', 'name')

app = web.application(urls, globals())

class index(object):
        def GET(self):
                greeting = 'Hello World'
                return greeting

class name(object):
        def GET(self):
            return "Brad"

if __name__ == '__main__':
    app.run()
