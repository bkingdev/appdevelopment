from sys import argv

script, input_file = argv

class print_functions:
	def __init__(self, file):
		self.file = file
	def print_all(self):
		print open(self.file).read()
	def rewind(self):
		print open(self.file).seek(0)
	def print_a_line(self, line_count):
		print line_count, open(self.file).readline()

print_funcs = print_functions(input_file)
print_funcs.print_all()
print_funcs.print_a_line(1)
print_funcs.print_a_line(2)
print_funcs.rewind()

#this script is kind of bunk because I put all functions
#into one class.  Example is intended to call open() at
#beginning of script and progress / regress the next line / #character, but since all in one class. open() is called
# on 4 instances, resetting the next char to read. 



