ten_things = "Apples Orange Crows Telephone Light Sugar"

print "Wait there's not 10 things in the list. Let's fix it."

stuff = ten_things.split(' ')
more_stuff = ["Day", "Night", "Song", "Frisbee", "Corn", "Banana", "Girl", "Boy"]
while len(stuff) != 10:
    next_one = more_stuff.pop()
    print "Adding", next_one
    stuff.append(next_one)
    print "There's %d items now" %len(stuff)
    
print "There we go : ", stuff

print "Let's do some stuff with stuff"

print stuff[1]
print stuff[-1]
print stuff.pop()
print len(stuff)
print stuff.pop()
print len(stuff)
print ' '.join(stuff)
print '#'.join(stuff[3:5])
joiner = "&&&&&"
print joiner.join(stuff)
