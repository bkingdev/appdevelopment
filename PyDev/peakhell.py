#!/usr/bin/env python

import urllib2
import pickle

url = 'http://www.pythonchallenge.com/pc/def/banner.p'

usock = urllib2.urlopen(url)
unpickled_data = pickle.load(usock)
usock.close()
print unpickled_data
for lst in unpickled_data:
    print_string = ''
    for k,p in lst:
        print_string += k*p
    print print_string

