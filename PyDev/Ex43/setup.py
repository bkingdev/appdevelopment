#!/usr/bin/env python

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = [
        'description':'My Project',
        'author':'Brad King',
        'url':'URL to get it at',
        'download_url':'Where to download it',
        'author_email':'bkinglouky@gmail.com',
        'version':'0.0.0.1',
        'install_requires': ['nose'],
        'packages' : ['NAME'],
        'scripts' : [],
        'name':'projectname'
        ]

setup(**config)

