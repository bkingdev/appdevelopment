from os import walk
import os.path
import sys

def read_file(path):
    with open(path) as fi:
        return fi.read()

path = sys.path[0] + '/challenge/'
files = [file_name for directory, directories, file_name in walk(path)]
numerals = "0123456789"
initial_file = "90052.txt"
messages = []
file_path = path + initial_file
used_file_names = []
while os.path.isfile(file_path):
    msg = read_file(file_path)
    
    if initial_file in file_path:
        messages.append(initial_file + " -> " + msg)
    else:
        messages.append(next_file + " -> " + msg)
    next_file = ''.join([numeric for numeric in msg if numeric in numerals]) + ".txt"
    file_path = path + next_file
    messages.append(msg)
    used_file_names.append(next_file)

for msg in messages:
    print msg

#for fn in files[0]:
#    if fn not in used_file_names:
#        print fn
#

