#!/usr/bin/env python

class CleaningTarget(object):
    is_completed = False
    notes = ''
 
    def __init__(self, id, target_date, goal):
        self.id = id
        self.target_date = target_date
        self.goal = goal

    def update_target_date(self, new_target_date):
        self.target_date = new_target_date

    def modify_goal(self, new_goal):
        self.goal = new_goal

    def mark_as_completed(self, notes):
        
        if notes.trim() == '':
            print "You must attach notes to complete this task."
        else:
            self.notes = notes
            self.is_completed = True

    def __str__(self):
        return """
            Goal Date: %s
            Goal: %s
            Completed: %s
            Notes: %s""" % (self.target_date, self.goal,
                          self.is_completed, self.notes)
    


