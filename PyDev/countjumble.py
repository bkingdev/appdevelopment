import urllib2
import re

url = 'http://www.pythonchallenge.com/pc/def/equality.html'

usock = urllib2.urlopen(url)

data = usock.read()
usock.close()

uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowers = uppers.lower()
valid_lowers = []
upper_indexes = []
lower_indexes = []
sequences = []
for i in range(len(data)):
    if data[i] in uppers:
        upper_indexes.append(i)
    elif data[i] in lowers:
        lower_indexes.append(i)

print lower_indexes

for lower in lower_indexes:
    lower_index_low = lower -4 
    lower_index_high = lower +4

    if data[lower_index_low] in uppers or data[lower_index_high] in uppers:
        continue

    sequence = [data[r] for r in range(lower_index_low, lower_index_high+1)]
    not_surrounded = sequence[0] not in uppers and sequence[len(sequence)-1] not in uppers
    left_is_upper = sequence[1] in uppers and sequence[2] in uppers and sequence[3] in uppers
    right_is_upper = sequence[5] in uppers and sequence[6] in uppers and sequence[7] in uppers
    if not_surrounded and left_is_upper and right_is_upper:
        print "Sequence is %s " % ''.join(sequence)
        print "Lower is %s " % lower
        valid_lowers.append(data[lower])

lowers_concat = ''.join(valid_lowers)
print lowers_concat
print 'finished seeking...'
    


            
        

