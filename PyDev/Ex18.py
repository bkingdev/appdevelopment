
def print_two(*args):
	arg_output = ""
	for arg in args:
		arg_output = arg_output + ">"+ arg
	print arg_output
	

def print_two_again(arg1, arg2):
	print "arg1: %r, arg2: %r" % (arg1, arg2)

def print_one(arg1):
	print "arg1 : %r" % arg1

def print_none():
	print "I got nothin'"

print_two("Zed", "Shaw", "Bob")
print_two_again("Brad", "King")
print_one("Anthony")
print_none()
