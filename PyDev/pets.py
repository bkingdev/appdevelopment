class Pet:
	def __init__(self, name, age):
		self.name = name
		self.age = age

class Cat(Pet):
	
	def __init__(self, name, age):
		super().__init__(name, age) #calling base Pet constructor

def Main():
	thePet = Pet('Pet', 1)
	jess = Cat('Jess', 3)

	print('is jess a cat: ' + str(isinstance(jess, Cat)))
	print('is jess a pet: ' + str(isinstance(jess, Pet)))
	print('is Pet a cat:' + str(isinstance(thePet, Cat)))

Main()
