import random
from random import randint

class Scene(object):
    def enter(self):
        print "This scene is not yet configured. Subclass it and implement enter()."
        
class Engine(object):
    def __init__(self, scene_map):
        self.scene_map = scene_map
        
    def play(self):
        current_scene = self.scene_map.opening_scene()
        
        while True:
            print "\n--------"
            next_scene_name = current_scene.enter()
            current_scene = self.scene_map.next_scene(next_scene_name)
            
class Death(Scene):
    quips = [
        "You died. You kinda suck at this.",
        "Your mom would be proud...if she were smarter",
        "Such a luser.",
        "I have a small puppy that's better at this."
    ]
    
    def enter(self):
        print Death.quips[randint(0, len(self.quips)-1)]
        exit(1)
        
class CentralCorridor(Scene):
    def enter(self):
        print """
            The Gothons of Planet Percal #25 have invaded your 
            ship and destroyed your entire crew. You are the 
            last surviving member and your last mission is to
            get the neutron destruct bomb from the weapons armory,
            put it into an escape pod."""
        print """
            As you stand to you stand to your feet, you notice
            a large rupture of twisted metal forms a passage into
            the central corridor. Whisps of smoke seep from the
            still orange glowing metal of the quarter wall. As 
            your shadow dances on the floor, you notice the wall
            behind you is engulfed in flames. With out another
            thought, you dive through the twisted metal passage
            into the central corridor."""
        print """
            As you tumble into the hallway, a scaley green   
            creature clad in an orange robe, stands looking 
            towards you at the end of the hall. What do you do?"""
        
        action = raw_input("> ")
        
        if action == "shoot!":
            print """
                Quick on the draw you yank out your blaster and
                fire it at the Gorrton. HIs clown costume is flowing
                and moving around his body, which throws off your
                aim. Your laser hits his costume but misses him 
                entirely. This completely ruins his costume but
                misses him entirely. This makes him fly into a
                rage and and blast you repeatedly in the face until
                you are dead. Then he eats you."""
            return 'death'
        elif action == "dodge!":
            print """
                Like a world class boxer, you dodge as the Gothon's
                Blaster cranks a laser past your head. In the middle
                of your artful dodge your foot slips and you bang
                your head into a metal wall and pass out.
                You wake up shortly after only to die as the Gothon
                stomps on your head and eats you."""
            return "death"
        elif action == "tell a joke":
            print """
                Lucky for you they made you learn Gothon  insults
                in the academy. You tell the one Gothon a joke
                you know:
                "Lbhe asok alsdo popqe, fur asap jio xio"
                The Gothon stops, tries not to laugh, then busts
                out laughing and can't move. While he's laughing
                you run up and shoot him square in the head putting
                him down, then jump through the Weapon Armory door.
                """
            return "laser_weapon_armory"
        else:
            print  "DOES NOT COMPUTE!"
            return "central_corridor"
            
        
class LaserWeaponArmory(Scene):
    def enter(self):
        print """
            You do a dive roll into the armory, crouch and scan
            the room for hiding Gothons. It's dead quiet.
            Too quiet. You stan up and run to the far side of
            the room and find the neutron bomb in it's container.
            There's a keypad lock on the box and you need the code
            to get the bomb out. If you get the code wrong 10 
            times then the lock closes forever."""
        code = "%d%d%d" % (randint(1,9), randint(1,9), randint(1,9))
        print code
        guess = raw_input("[keypad]> ")
        guesses = 0
        while guess != code and guesses < 10:
            print "BZZZZZED"
            guesses += 1
            guess = raw_input("[keypad]> ")
        
        if guess == code:
            print """
                The container clicks popen and the seal breaks,
                letting gas out. You grab the neutron bomb and
                run as fast as you can to the bridge where you
                must lace it in the right spot."""
            return "the_bridge"
        else:
            print """
                The lock buzzes one last time and then you hear a
                sickening melting sound as the mechanism is fused
                together. You decide to sit there, and finally
                the Gothons blow up the ship from their ship
                and you die."""
            return "death"
             

class TheBridge(Scene):
    def enter(self):
        print """
            You burst onto the Bridge with the neutron destruct
            bomb under your arm and surprise 5 Gothons who are
            trying to take control of the ship. Each of them has
            an even uglier clown costume than the last. They 
            haven't pulled their weapons out yet, as they see the
            active bomb under your arm and don't want to set it
            off."""
        
        action = raw_input("> ")
        
        if action == "throw the bomb":
            print """
                In a panic you throw the bomb at the group of
                Gothons and make a leap for the door. Right as you
                drop it a Gothon shoots you right in the back
                killing you. As you die you see another Gothon
                frantically tyry to disarm thhe bomb. You know they
                will probably die when it goes off."""
            return "death"
        elif action == "slowly place the bomb":
            print """
                You point your blaster at the bomb under your arm
                and the Gothons put their hands up and start to 
                sweat. You inch backward towaeds the door, open it
                , and then carefully place the bomb on the floor,
                poitnting your blaster at it. You then jump back
                through the door, punch the close button and blast
                the lock so Gothons can't get out. Now that the 
                bomb is set to explode, you run to the escape pod
                to get off this tin can."""
            return "escape_pod"
        else:
            print "DOES NOT COMPUTE"
            return "the_bridge" 
        
class EscapePod(Scene):
    def enter(self):
        print """
            You rush through the door desperately trying to make
            it to the escape pod before the ship explodes. It seems
            like hardly any Gothons are on the ship, so your run
            is clear of interference. You get to the chamber
            with the escape pods, and now need to pick one to take.
            Some of them could be damaged. You don't have time to 
            look. There are 5 pods. Which one do you take?"""
        
        good_pod = randint(1,5)
        print "The pod in bay %d looks unscathed." %good_pod
        guess = raw_input("[pod #]> ")
        
        if int(guess) != good_pod:
            print """
                You jump into %s pod and hit the eject button.
                The pod escapes out into the void of space, then
                implodes as the hull ruptures, crushing you into
                jelly jam.""" % guess
            return "death"
        else:
            print """
                You jump into pod %s and hit the eject button.
                The pod rumbles, then rapidly increases speed,
                flinging itself into space beyond. Looking back,
                you see your ship explode in a violent exhibition
                of electronic nebula. You win.""" % guess
            return "finished"
            
class Finished(Scene):
    def enter(self):
        print """
            This is a demo game. 
            It was Reproduced from 'Learn Python the Hard Way'.
            Thanks for playing"""
        print "\nCoded by Brad King"
        quit(0)
        
                

class Map(object):
    scenes = {
        "central_corridor":CentralCorridor(),
        "laser_weapon_armory": LaserWeaponArmory(),
        "the_bridge":TheBridge(),
        "escape_pod":EscapePod(),
        "death":Death(),
        "finished": Finished()
    }
    
    def __init__(self, start_scene):
        self.start_scene = start_scene
    
    def next_scene(self, scene_name):
        return Map.scenes.get(scene_name)

    def opening_scene(self):
        return self.next_scene(self.start_scene)

a_map = Map('central_corridor')
a_game = Engine(a_map)
a_game.play()
