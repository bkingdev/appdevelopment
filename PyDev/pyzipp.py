import zipfile, re

idx = "90052"
fi = zipfile.ZipFile("channel.zip", "r")
history = []

while True:
    history.append(idx)
    data = fi.read(idx+'.txt')
    print "File", idx + ":\t" + data
    idx = "".join(re.findall('[0-9.]', data))
    if len(idx) == 1:
        break

print ''.join([fi.getinfo(i+'.txt').comment for i in history])
