
class Recruitment(object):

    def __init__(self, recruit, recruit_guardian, recruit_environment):
        self.recruit = recruit
        self.recruit_guardian = recruit.guarian
        self.recruit_environment  = recruit_recruit_environment


class RecruitEnvironment(object):

    def __init__(self, city, proximity_to_school, wealth_level):
        self.proximity_to_school  = proximity_to_school
        self.wealth_level = wealth_level

    def write_city_scene(self):
        pass

    def write_home_scene(self):
        pass

class PlayerAttributes(object):

    def __init__(self, position, height, skill_level):
        self.position = position
        self.height = height
        self.skill_level = skill_level

class RecruitTarget(object):
    impressment = 0
    impressment_threshold = 10

    def increase_impressment(self, amt):
        self.impressment += amt

    def decrease_impressment(self, amt):
        self.impressment -=  amt


class Trait(RecruitTarget):

    def __init__(self, trait_name, trait_level, related_traits):
        self.trait_name = trait_name
        self.trait_level = trait_level
        self.related_traits = related_traits

class Player(RecruitTarget):

    def __init__(self, name, player_attributes, traits):
        self.name = name
        self.player_attributes = player_attributes
        self.traits = traits

class PlayerGuardian(object):

    def __init__(self, name, relation_to_player, traits):
        self.name = name
        self.relation_to_player = relation_to_player
        self.traits = traits
        super(RecruitTarget, self).impressment_threshold = 5



