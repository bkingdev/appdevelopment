import random

class Randomizer(object):
    
    DEVIANCY = 2

    def randomize_traits(traits):
        for trait in traits:
            if(trait.trait_level != 0):
                continue
            else:
                trait.trait_level =  randint(1, 10)
                for related_trait in traits.related_traits:
                    set_related_trait_level(traits, related_trait, trait.trait_level)

    def set_related_trait_level(traits, related_trait, related_level):
        seek_trait(traits, related_trait).trait_level = randint(related_level-DEVIANCY, 
                                                                related_level+DEVIANCY)

    def seek_trait(traits, name):
        traitDict = (dict(trait.name, trait) for trait in traits)
        return traitDict[name]
