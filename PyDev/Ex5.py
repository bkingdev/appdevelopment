my_name = "Brad King"
my_age = 30 #not a lie
my_height = 74 # inches
my_weight = 300 #lbs
my_eyes = "Blue"
my_teeth = "White"
my_hair = "Brown"

print "Let's talk about %s" %my_name
print "He's %d inches tall" %my_height
print "He's %d pounds heavy" %my_weight
print "Actually, it's pretty heavy"
print "He's got %s eyes and %s hair" %(my_eyes, my_hair)
print "His teeth are usually %s depending on the coffee" %my_teeth

#tricky line
print "if I add %d, %d and %d I get %d" %(my_age, my_height, my_weight, my_age + my_height + my_weight)
