#!/usr/bin/env python

class Parent(object):

    def override(self):
        print 'PARENT override()'

    def implicit(self):
        print 'PARENT implicit()'

    def altered(self):
        print 'Parent altered()'

class Child(Parent):

    def override(self):
        print 'Child override()' #Straight up override of parent

    def altered(self):
        print 'CHILD BEFORE PARENT altered()' #This is overriding parent's altered behaviore
        super(Child, self).altered() #Calls parent's altered() -> Technically overriding then calling same method of parent
        print 'CHILD, AFTER PARENT altered()' #Still overriding

dad = Parent()
son = Child() #Child inherits from Parent

dad.implicit() 
son.implicit() #This is complete inheritance w/ out alteration or override

dad.override()
son.override() #Completely override's method of father with new method of same name

dad.altered() 
son.altered() #Completely override's method of father with method of same name BUT ALSO calls father's method of same name
