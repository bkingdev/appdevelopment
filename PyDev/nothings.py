import urllib2
import re

numerals = '123456789'
base_url = 'http://www.pythonchallenge.com/pc/def/linkedlist.php?'
def get_string(url):
    usock = urllib2.urlopen(url)
    data = usock.read()
    usock.close()
    return data

def get_numerals(html_string):
    return ''.join( html_string[i] for i in range(len(html_string)) if html_string[i] in numerals)

extension_list = []
count = 400
extension = '12345'
something_html = ''
for i in range(count):
    url = "http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing="
    url = url + extension
    base_string = get_string(url)
    print base_string
    extension = ''.join(re.findall(r"nothing is (\d+)", base_string))
    extension_list.append(extension)

somethings = set([ext for ext in extension_list if len(ext) < 4])
print sum([int(ext) for ext in somethings])

response = raw_input("> ")

