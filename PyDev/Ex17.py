#!/usr/bin/python

from sys import argv
from os.path import exists

script, from_file, to_file = argv
user_input_type = raw_input(
	"Type 'A' to Append or 'W' to Overwrite > \t")
write_type = "a" if user_input_type.upper() == "A"else "w"
indata = open(from_file).read()
out_file = open(to_file, write_type).write(indata)
print "Completed."
