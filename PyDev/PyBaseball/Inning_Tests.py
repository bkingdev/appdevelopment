import unittest
from Inning import Inning

class Test_Inning(unittest.TestCase):

    def test_increase_run(self):
        i = Inning()
        i.increase_runs(5)
        self.failUnlessEqual(5, i.runs)


if __name__ == '__main__':
    unittest.main()
