import unittest
from Diamond import Diamond

class test_advance_runners(unittest.TestCase):

    def test_man_on_first(self):
        d = Diamond()
        d.add_runner(0)
        self.failUnlessEqual(True, d.bases_occupied[0])

    def test_advance_to_second_man_on_first(self):
        d = Diamond()
        d.add_runner(0)
        d.advance_runners(1)
        self.failUnlessEqual(True, d.bases_occupied[1])
    
    def test_advance_third_to_scored_run_and_clear_third(self):
        d = Diamond()
        d.add_runner(2)
        self.failUnlessEqual(1, d.advance_runners(1))
        self.failUnlessEqual(False, d.bases_occupied[2])
    
    def test_do_runners_clear_after_home_run(self):
        d = Diamond()
        d.add_runner(0)
        d.add_runner(1)
        d.add_runner(2)
        self.failUnlessEqual(3, d.advance_runners(4))
        self.failUnlessEqual(False, d.bases_occupied[0])
        self.failUnlessEqual(False, d.bases_occupied[1])
        self.failUnlessEqual(False, d.bases_occupied[2])


            
if __name__ == '__main__':
    unittest.main()

