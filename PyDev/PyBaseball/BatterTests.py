import unittest
from Dice import Dice
from Inning import Inning
from Batter import Batter
from Diamond import Diamond

class TestBatter(unittest.TestCase):

    def test_hit(self):
        d = Diamond()
        dice = Dice(6)
        inning = Inning()
        batter = Batter(d, dice, inning)
        batter.hit(1)
        self.failUnlessEqual(True, batter.on_base)
    def test_hit_and_score(self):
        d = Diamond()
        dice = Dice(6)
        inning = Inning()
        batter = Batter(d, dice, inning)
        batter.hit(3)
        batter2 = Batter(d, dice, inning)
        batter2.hit(1)
        self.failUnlessEqual(1, inning.runs)

    def test_swing(self):
        counter = 0
        while counter < 50:
            d = Diamond()
            dice = Dice(6)
            inning = Inning()
            batter = Batter(d, dice, inning)
            batter.swing()
            print batter
            self.failUnlessEqual(True, (batter.on_base or batter.is_out or batter.strikes == 1))
            counter +=1

if __name__ == '__main__':
    unittest.main()
