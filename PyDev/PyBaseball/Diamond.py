
class Diamond(object):
    bases_occupied =  [False, False, False]
    
    def get_base_name(self, index_num):
        if index_num == 0:
            return "First"
        elif index_num == 1:
            return "Second"
        elif index_num == 2:
            return "Third"
        else:
            return "Home"

    def remove_runner(self, arrayPosition):
        self.bases_occupied[arrayPosition] = False

    def add_runner(self, arrayPosition):
        self.bases_occupied[arrayPosition] = True

    def advance_runners(self, numberToAdvance):
        counter = 0
        runs_scored = 0
        max_bases = len(self.bases_occupied)
        diamond_result = [False] * len(self.bases_occupied)
        
        while counter < max_bases:
            current_base = self.get_base_name(counter)
            future_base_index = counter + numberToAdvance
            future_base = self.get_base_name(future_base_index)

            if self.bases_occupied[counter] == False:
                counter += 1
                continue

            if counter + numberToAdvance  >= max_bases:
                runs_scored += 1
                print 'Runner on %s Scores a Run' % current_base
                counter += 1
                continue
            
            print 'Runner on %s advances to %s' % (current_base, future_base)
            diamond_result[future_base_index] = True
            counter += 1

        self.bases_occupied = diamond_result
        return runs_scored
    

    def report_bases(self):
        runner_statement = ("Runners" if self.bases_occupied.count(True) > 1 else "Runner") + " on:  "
        runner_statement = runner_statement +  ("First " if self.bases_occupied[0] == True else "")
        runner_statement = runner_statement + ("Second " if self.bases_occupied[1] == True else "")
        runner_statement = runner_statement + ("Third " if self.bases_occupied[2] == True else "")
        print runner_statement
        




