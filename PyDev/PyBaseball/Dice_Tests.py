import unittest
from Dice import Dice

class Test_Dice(unittest.TestCase):

    def test_6_sided(self):
        counter = 0
        while counter > 20:
            sides = 6
            d = Dice(sides)
            roll_val = d.Roll()
            print 'Roll Val: %r' % roll_val
            self.failUnlessEqual(True, (roll_val >= 1 and roll_val <= sides))
            counter += 1


if __name__ == "__main__":
    unittest.main()
