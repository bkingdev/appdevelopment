import random

class Dice(object):
    sides = 6

    def __init__(self, sides = None):
        self.sides = 6 if sides == None else sides

    def roll(self):
        return random.randint(1, self.sides)

