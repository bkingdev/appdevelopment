from Dice import Dice
from Inning import Inning
from Diamond import Diamond
from Batter import Batter

class PyBallEngine(object):
    innings = 6
    inning_dict = {}
    dice = Dice(6)
    
    def __init__(self):
        pass

    def play_game(self):

        for inningCount in range(self.innings):
            inning = Inning()
            inning.score = self.play_inning(inning)
            self.inning_dict[inningCount] = inning
            print "Inning %s is over" % (inningCount + 1)
            self.write_scoreboard()

    def play_inning(self, inning):
        outs = 0
        diamond = Diamond()
        while(outs < 3):
            batter = Batter(diamond, self.dice, inning)
            batter.bat()
            if batter.is_out:
                outs += 1
    
    def write_scoreboard(self):
        print "*" * 20
        total = 0
        for k,v in self.inning_dict.iteritems():
            print "Inning %s -> Scored %s" % (k+1, v.runs)
            total += v.runs
        print "TOTAL RUNS %s" % total
        print "*" * 20



if __name__ == "__main__":
    engine = PyBallEngine()
    engine.play_game()
