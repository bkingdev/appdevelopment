In 2010, attempted to make a baseball game in C#. Lacked general programming knowledge to accomplish task.

Game should be predicated on two roll of 6 sided dice, one blue and one green.

Blue Dice: 1-4 is a strike. 5-6 is a hit. 
Green Dice: Multiplier Die -> 
                                Snakeyes: Double Play if player on base
                                Twos: ground out
                                Threes: Fly Out runner advances if on base
                                Fours: Double
                                Fives: Triple
                                Sixes: Homerun

 A game consists of 6 innings to score maximum points. Three outs ends the inning. 

Game could consist of two main components. A dice interpreter which gives commands to engine. An engine which inteprets commands.

The engine should be responsible for managing inning, outs, score, and baserunners. 

Essentially, the engine stores the objects which are moved around by the interpreter. 

Possible enhancements could involve strategization, including stealing bases, big swings, etc.





