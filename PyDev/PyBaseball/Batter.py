from ConfigParser import ConfigParser
import sys

class Batter(object):
    is_out = False
    strikes = 0
    on_base = False
    rules = ConfigParser()
    rules.read('DiceRules.cfg')
    
    def __init__(self, diamond, dice, inning):
        self.diamond = diamond
        self.dice = dice
        self.inning = inning
       
    def __str__(self):
        return 'Strikes: %s /n Is Out: %s /n On Base: %s' % (self.strikes, self.is_out, self.on_base)

    def bat(self):

        while(self.strikes < 3 and self.on_base == False and self.is_out == False):

            self.print_batting_options()
            player_choice = raw_input("> ")
        
            if player_choice == "S":
                self.swing()
            elif player_choice == "SC":
                print "TODO"
            elif player_choice == "Q":
                sys.quit(0)


    def print_batting_options(self):
        bar = "*" * 15
        print bar
        print "Press 'S' to Swing"
        print "Press 'Q' to Quit"

        if(any(self.diamond.bases_occupied)):
            print "Press 'ST' to Steal"
        
        print "Press 'SC' for Score"
        print bar
    
    def swing(self):
        roll_val_one = str(self.dice.roll())
        roll_val_two = str(self.dice.roll())
        print 'You rolled a %s and a %s' % (roll_val_one, roll_val_two) 
        if roll_val_one == roll_val_two:
            double_result = self.rules.get('Doubles', roll_val_one)
            
            if double_result == 'X':
                self.is_out = True
                print "A long drive deep to center field is robbed at the fence! You're outta here!"

            elif double_result == '4':
                print 'HOMERUN!!!!!!!!!!!!'
                self.inning.increase_runs(self.diamond.advance_runners(4)+1)
                self.on_base = True
        
        else:

            roll_one_result = self.rules.get('DiceOne', roll_val_one)
            
            if roll_one_result == 'X':
                self.strikes += 1
                print 'Steeerike %s' % self.strikes
                if self.strikes == 3:
                    self.is_out = True
                    print "You're out!"
                elif self.strikes < 3:
                    pass
                else:
                    print 'Something is wrong. Batter has more than 3 strikes'

            
            elif roll_one_result == 'O':
                roll_two_result = self.rules.get('DiceTwo', roll_val_two)
                if roll_two_result == 'X':
                    print self.rules.get('DiceTwoDescriptions', roll_val_two)
                    self.is_out = True
                    print "You're out!"
                else:
                    hit_val = int(roll_two_result)
                    self.hit(hit_val)
            else:
                hit_val = int(roll_one_result)
                self.hit(hit_val)

    def hit(self, bases):
        self.inning.increase_runs(self.diamond.advance_runners(bases))
        self.diamond.add_runner(bases -1)
        self.on_base = True
        print 'Hitter advances to %s' % self.diamond.get_base_name(bases-1)



        

            

