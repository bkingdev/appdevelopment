from django.shortcuts import render
from django.http import HttpResponse
from TasksManager.models import Supervisor, Developer
from django import forms

error_name = {
    'required' : 'You must type a name.',
    'invalid' : 'Wrong format.'
}

class Form_inscription(forms.Form):
    name = forms.CharField(label='Name', max_length=30, error_messages=error_name)
    login = forms.CharField(label='Login', max_length=30)
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    password_bis = forms.CharField(label='Password', widget=forms.PasswordInput)
    supervisor = forms.ModelChoiceField(label='Supervisor', queryset=Supervisor.objects.all())

    def clean(self):
        cleaned_data = super(Form_inscription, self).clean()
        password = self.cleaned_data.get('password')
        password_bis = self.cleaned_data.get('password_bis')
        if password and password_bis and password != password_bis:
            raise forms.ValidationError('Passwords are not identical')

        return self.cleaned_data

def page(request):
    if request.POST:
        form = Form_inscription(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            login = form.cleaned_data['login']
            supervisor = form.cleaned_data['supervisor']
            new_user = User.objects.create_user(username = login, password=password)
            new_user.is_active = True
            new_user.last_name = name
            new_user.save()
            new_developer = Developer(name=name, login=login, password=password ,email='', supervisor = supervisor)
            new_developer.save()
            return HttpResponse('developer added')
        else:
            return render(request, 'en/public/create_developer.html', {'form': form})
    else:
        form = Form_inscription()
        return render(request, 'en/public/create_developer.html', {'form': form})


