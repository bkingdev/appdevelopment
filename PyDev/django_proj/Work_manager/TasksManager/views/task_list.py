from django.shortcuts import render
from TasksManager.models import Task
from django.core.urlresolvers import reverse

def page(request):
    tasks_list = Task.objects.all()
    last_task = 0
    if 'last_task' in request.session:
        last_task = Task.objects.get(id = request.session['last_task'])
        tasks_list = tasks_list.exclude(id = request.session['last_task'])
    return render(request, 'en/public/tasks_list.html', {'tasks_list':tasks_list, 'last_task' : last_task})

