from django.views.generic import DetailView
from TasksManager.models import Developer, Task

class Developer_detail(DetailView):
    model = Developer
    template_name = 'en/public/developer_detail.html'
    
    def get_context_data(self, **kwargs):
        context = super(Developer_detail, self).get_context_data(**kwargs)
        tasks_dev = Task.objects.filter(developers=self.object)
        context['tasks_dev'] = tasks_dev
        return context




