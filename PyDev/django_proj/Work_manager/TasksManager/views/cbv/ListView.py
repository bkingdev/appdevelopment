from django.views.generic.list import ListView

from TasksManager.models import Project

class Project_list(ListView):
    model = Project
    template_name = 'en/public/project_list.html'
    paginate_by = 5

    def get_queryset(self):
        queryset = Project.objects.all().order_by('title')
        return queryset


