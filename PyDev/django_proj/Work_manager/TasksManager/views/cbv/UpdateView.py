from django.views.generic import UpdateView
from TasksManager.models import Task
from django.forms import ModelForm
from django.core.urlresolvers import reverse

class Form_task_time(ModelForm):
    class Meta:
        model = Task
        fields = ['time_elapsed']

class Task_update_time(UpdateView):
    model = Task
    template_name = 'en/public/update_task_developer.html'
    success_url = 'public_empty'
    def get_success_url(self):
        return reverse(self.success.url)

