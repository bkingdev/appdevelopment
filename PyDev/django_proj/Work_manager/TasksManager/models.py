from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):

    def __str__(self):
        return self.user_auth.username

    error_name = {
        'required':'You must type a name',
        'invalid':'Wrong format.'
    }

    user_auth = models.OneToOneField(User, primary_key=True)
    phone = models.CharField(max_length=20, verbose_name="Phone Number", null=True, default=None, blank=True)
    born_date = models.DateField(verbose_name="Born Date", null=True, default=None, blank=True)
    last_connection = models.DateTimeField(verbose_name="Date of Last Connection", null=True, default=None, blank=True)
    years_seniority = models.IntegerField(verbose_name="Seniority", default=0)

class Project(models.Model):

    def __str__(self):
        return self.title

    title = models.CharField(max_length=50, verbose_name="Title")
    description = models.CharField(max_length=1000, verbose_name="Description")
    client_name = models.CharField(max_length=1000, verbose_name="Client name")

class Supervisor(UserProfile):
    def __str__(self):
        return self.user_auth.username

    specialisation = models.CharField(max_length=50, verbose_name = "Specialisation")

class Developer(UserProfile):
    supervisor = models.ForeignKey(Supervisor, verbose_name="Supervisor")

class Task(models.Model):
    title = models.CharField(max_length=50, verbose_name="Title")
    description = models.CharField(max_length=1000, verbose_name="Description")
    time_elapsed = models.IntegerField(verbose_name="Elapsed Time", null=True, default=None, blank=True)
    importance = models.IntegerField(verbose_name="Importance")
    project = models.ForeignKey(Project, verbose_name="Project", null=True, default=None, blank=True)
    app_user = models.ForeignKey(Developer, verbose_name="User", related_name="app_user")
    developers = models.ManyToManyField(Developer, through="DeveloperWorkTask", related_name="developer")

class DeveloperWorkTask(models.Model):
    developer = models.ForeignKey(Developer)
    task = models.ForeignKey(Task)
    time_elapsed_dev = models.IntegerField(verbose_name="Time Elapsed", null=True, default=None, blank=True)


