from django.conf.urls import patterns, include, url
from django.views.generic import CreateView
from TasksManager.models import Project, Task, Developer
from django.contrib import admin
from TasksManager.views.cbv.ListView import Project_list
from django.views.generic.list import ListView
from django.views.generic import DetailView
from TasksManager.views.cbv.DetailView import Developer_detail
from django.views.generic import UpdateView
from TasksManager.views.cbv.UpdateView import Task_update_time

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Work_manager.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','TasksManager.views.index.page'),
    url(r'^index$','TasksManager.views.index.page'),
    url(r'^$', 'TasksManager.views.index.page', name='public_index'),
    url(r'^connect$', 'TasksManager.views.connection.page', name='public_connection'),
    url(r'project-detail-(?P<pk>\d+)$', 'TasksManager.views.project_detail.page', name='project_detail'),
    url(r'^create-developer$', 'TasksManager.views.create_developer.page', name='create_developer'),
    url(r'^create-supervisor$', 'TasksManager.views.create_supervisor.page', name='create_supervisor'),
    url(r'^create-project$', CreateView.as_view(model=Project, template_name="en/public/create_project.html", success_url='index'), name='create_project'),
    url(r'^create-task$', CreateView.as_view(model=Task, template_name="en/public/create_task.html", success_url='index'), name='create_task'),
    url(r'^project_list$', Project_list.as_view(), name="project_list"),
    url(r'^developer_list$', 'TasksManager.views.task_detail.page', name='task_detail'),
    url(r'^task_detail_(?P<pk>\d+)$', DetailView.as_view(model=Task, template_name="en/public/task_detail.html"), name="task_detail"),
    url(r'^developer_detail_(?P<pk>\d+)$', Developer_detail.as_view(), name='developer_detail'),
    url(r'update_task_(?P<pk>\d+)$', UpdateView.as_view(model=Task, template_name="en/public/update_task.html", success_url="index"), name="update_task"),
    url(r'update_task_time_(?P<pk>\d+)$', Task_update_time.as_view(), name='update_task_time'),
    url(r'task_list$', 'TasksManager.views.task_list.page', name='task_list'),
    )

