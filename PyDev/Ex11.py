print "How old are you?",
age = raw_input()
print "How tall are you?",
height = raw_input()
print "How much do you weigh?",
weight = raw_input()
print "Type 'Y' for Raw String Type 'N' for User String"
raw_format = raw_input() == "Y"
rawString = """
So you're %r years old
You stand at %r
And you weight %r
""" if raw_format else """
So you're %s years old
You stand at %s
And you weigh %s"""

print rawString % (age, height, weight)
