using System;
using System.Collections.Generic;

namespace CommandPattern
{
	public interface ICommand
	{
		void Execute();
	}

	/*The invoker class */
	public class Switch
	{
		private List<ICommand> _commands = new List<ICommand>();

		public void StoreAndExecute(ICommand command)
		{
			_commands.Add(command);
			command.Execute();
		}
	}

	/*Receiver Class*/
	public class Light
	{
		public void TurnOn()
		{
			Console.WriteLine ("The light is on.");
		}

		public void TurnOff()
		{
			Console.WriteLine ("The ligh is off.");
		}
	}

	///Command for turning off light
	public class FlipDownCommand : ICommand
	{
		private Light _light;

		public FlipDownCommand(Light light)
		{
			_light = light;
		}

		public void Execute()
		{
			_light.TurnOff();
		}
	}

		///Command for turning off light
	public class FlipUpCommand : ICommand
	{
		private Light _light;

		public FlipUpCommand(Light light)
		{
			_light = light;
		}

		public void Execute()
		{
			_light.TurnOn();
		}
	}

	/*Test class of client*/
	internal class Program
	{
		public static void Main(string[] args)
		{
			var lamp = new Light();
			var switchUp = new FlipUpCommand(lamp);
			var switchDown = new FlipDownCommand(lamp);

			var lightSwitch = new Switch();
			var commandlineArg = args.Length() > 0 
				? args[0].ToUpper() 
					: string.Empty; 

			if(commandlineArg == "ON")
				lightSwitch.StoreAndExecute(switchUp);
			else
				lightSwitch.StoreAndExecute(switchDown);

		}
	}
}

