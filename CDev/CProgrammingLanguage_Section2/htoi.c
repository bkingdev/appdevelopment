#include <stdio.h>

int htoi(char s[]);
int atoid(char s[]);

void main()
{
	printf("%d \r\n\r\n", htoi("1"));
	printf("%d \r\n\r\n", htoi("1"));
	printf("%d \r\n\r\n", htoi("2"));
	printf("%d \r\n\r\n", htoi("3"));
	printf("%d \r\n\r\n", htoi("4"));
}

int htoi(char s[])
{
	int size = sizeof(s);
	int i = 0;
	int value = 0;
	for(i = 0; i < size; i++)
	{
		int subtractor = i +1; 
		int curDigit = s[size-subtractor] - '0';
		printf("Current digit is %d \r\n", curDigit);
		printf("Current value of i is %d \r\n", i);
		printf("i to the Sixteenth is %d \r\n", i^16);
		value += curDigit * (i^16);
	
	}
	return value;
}


int atoi(char s[])
{
	int i, n;
	n = 0;
	for(i = 0; s[i] >= '0' && s[i] <= '9'; ++i)
		n = 10 * n + (s[i] - '0');
	return n;
}
