#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

void printSize(char name[], int constant);
void printSizeUnsigned(char name[], int constant);

void main()
{
	printf("Char: \n");
	printSize("char", CHAR_BIT);
	printSize("max char", CHAR_MAX);
	printSize("min char", CHAR_MIN);
	printSize("max signed char", SCHAR_MAX);
	printSize("Min signed char", SCHAR_MIN);
//	printSizeUnsigned("Max unsigned char", UCHAR_MAX);
//	printSize("Int: \n");
//	printSize("Max int", INT_MAX);
//	printSize("Min int", INT_MIN);
//	printSizeUnsigned("Max uint", UINT_MAX);
//	printf("Long: \n");
//	printSize("Max long", LONG_MAX);
//	printSize("Min long", LONG_MIN);
//	printSizeUnsigned("Max ulong", ULONG_MAX);
//	printf("\nShort\n");
//	printSize("Min short", SHRT_MIN);
//	printSize("Max short", SHRT_MAX);
//	printSizeUnsigned("Max ushort", USHRT_MAX);
}

void printSize(char name[], int constant)
{
	printf("Size of %s: %d\n", name, constant);
}

void printSizeUnsigned(char name[], int constant)
{
	printf("Size of %s: %u\n", name, constant);
}
