#include <stdio.h>
#define TAB 3
void put_tab_spaces(void);
void flushbuffer(char buffer[]);

main()
{
	int c;
	int space_count;
	space_count = 0;
	char buffer[5];

	while((c = getchar()) != EOF){
		if(c == ' '){
			if(space_count == TAB){
				putchar('\t');
				space_count = 0;
			}
			else{
				buffer[space_count] = c;
				++space_count;
			}
		}
		else{
			if(space_count != 0){
				flushbuffer(buffer);
				space_count = 0;
			}
			putchar(c);
		}
	}
}

void flushbuffer(char buffer[])
{
	int i;
	for(i = 0; i < TAB; ++i){
		putchar(buffer[i]);
	}
	buffer = "";
}


