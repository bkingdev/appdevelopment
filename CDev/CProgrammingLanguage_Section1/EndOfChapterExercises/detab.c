#include <stdio.h>
#define TAB 3
void put_tab_spaces(void);


main()
{
	int c;

	while((c = getchar()) != EOF){
		if(c == '\t')
			put_tab_spaces();
		else
			putchar(c);
	}
}

void put_tab_spaces(void)
{
	int i;
	for(i = 0; i < TAB; i++){
		putchar(' ');
	}
}
